﻿using Microsoft.EntityFrameworkCore;

var context = new AppDbContext();

// EAGER loading
// load 1 related

var users = context.Users
                    .Include(c => c.Posts)
                    .ToList();

// Load multiple related

var loadMultiple = context.Users
                    .Include(c => c.Posts)
                    .Include(a => a.Address)
                    .ToList();

// Load Related of Related
var loadRelated = context.Users
                    .Include(c => c.Posts)
                        .ThenInclude(x => x.Tags)
                    .Include(a => a.Address)
                    .ToList();
// inner join
var userPosts = context.Users
                            .Join(
                                context.Posts,
                                user => user.Id,
                                post => post.UserId,
                                (userMatch, postMatch) => new {
                                                                        Cus = userMatch,
                                                                        Ord = postMatch
                                                                   }
                            )
                            .ToList();


// EXPLICIT loading
var user = context.Users.FirstOrDefault();
context.Entry(user).Reference(x => x.Address).Load();
context.Entry(user).Collection(x => x.Posts).Load();

// EXPLICIT loading
var user1 = context.Users.FirstOrDefault();
context.Entry(user).Reference(x => x.Address).Load();
context.Entry(user).Collection(x => x.Posts)
                    .Query()
                    .Include(x => x.Tags)
                    .ToList();
